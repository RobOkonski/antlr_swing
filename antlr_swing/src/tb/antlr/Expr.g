grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr) // dodano instrukcję print
    | STARTBLOCK NL -> STARTBLOCK  // obsługa bloków
    | ENDBLOCK NL -> ENDBLOCK
    | ifStat NL -> ifStat
    | NL ->
    ;

ifStat
    : IF^ compExpr THEN! stat (ELSE! stat)?
    ;

compExpr
    : expr
    ( EQU^ expr
    | NEQ^ expr
    )
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;
    
powerExpr
    : atom
    ( POW^ atom )* // ptęgowanie
    ;

// zamiana atom na powerExpr bo najpierw potrzeba zpotęgować a potem wykonać inne działania
multExpr
    : powerExpr
      ( MUL^ powerExpr
      | DIV^ powerExpr
      | MOD^ powerExpr // działanie modulo
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
    
STARTBLOCK : '{';  // otwarcie bloku

ENDBLOCK : '}';  // zamknięcie bloku

VAR :'var';

PRINT : 'print'; //komenda wypisania

IF : 'if'
   ;
   
THEN : 'then'
     ;
 
ELSE : 'else'
     ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

EQU
  : '=='
  ;
  
NEQ
  : '!='
  ;  
  
PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

MOD
  : '%'  // znak modulo
  ;
  
POW
  : '^'  // znak potęgi
  ;