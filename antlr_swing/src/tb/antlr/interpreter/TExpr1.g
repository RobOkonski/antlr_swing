tree grammar TExpr1;

// Na laboratorium 2 wykonano:
// - dodano brakujące akcje do instrukcji
// - zdefiniowano instrukcje wypisania print(bez niej interpreter nie wypisze wyniku)
// - dodano dodatkowe działania (modulo, potęgowanie)
// - obsługa zmiannych lokalnych + bloków instrukcji

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
  superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

// zdefiniowno instrukcje print i obsługę bloków instrukcji
prog    : 
        ( ^(VAR i1=ID) {createVariable($i1.text);}
        | ^(PODST i1=ID   e2=expr) {setVariable($i1.text,$e2.out);}
        | ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());} 
        | STARTBLOCK {symbols.enterScope();}
        | ENDBLOCK {symbols.leaveScope();}
        )* 
        ;

// zdefiniowano akcje dla działań + dodano działanie modulo i potęgowanie
expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = (int)Math.pow($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getValue($ID.text);}
        ;
