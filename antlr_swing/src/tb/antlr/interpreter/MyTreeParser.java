package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {

	//deklaracja pamieci
	protected LocalSymbols symbols = new LocalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	//przypisanie symbolu
	protected Integer setVariable(String name, Integer value) {
		symbols.setSymbol(name, value);
		return value;
	}
	
	// zwrócenie wartości symbolu
	protected Integer getValue(String name) {
		return symbols.getSymbol(name);
	}
	
	// stworzenie symbolu
	protected void createVariable(String name) {
		symbols.newSymbol(name);
	}
}
